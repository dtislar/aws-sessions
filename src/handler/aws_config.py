import configparser

def get_profiles(file_path):
    config = configparser.RawConfigParser()
    try:
        mfa_enabled_profiles = []
        config.read(file_path)
        for profile in config:
            if 'mfa_serial' in config[profile]:
                mfa_enabled_profiles.append(profile)
        return mfa_enabled_profiles
    except configparser.NoOptionError:
        return None
    
def get_profile_details(file_path, profile):
    """
    Reads role arn from AWS AWS_CONFIG_FILE if exists

    :type file: string
    :param file: Credentials file name to be used

    :type profile: string
    :param profile: Title of the profile from which to read details
    """
    config = configparser.RawConfigParser()
    try:
        config.read(file_path)
        role = config.get('profile %s' % profile, 'role_arn')
        source_profile = config.get('profile %s' % profile, 'source_profile')
        mfa_serial = config.get('profile %s' % profile, 'mfa_serial')
        return role, source_profile, mfa_serial
    except configparser.NoSectionError as err:
        print('\nProfile %s does not exists in %s' % (profile, file))
        print("\n%s, Exiting" % err, file=stderr)
        sysexit(1)
    except configparser.NoOptionError:
        return None

# window.py
#
# Copyright 2020 Dario Tislar
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import path
from gi.repository import Gtk
from aws_sessions.handler import aws_config


@Gtk.Template(resource_path='/io/gitlab/dtislar/aws_sessions/window.ui')
class AwsSessionsWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'AwsSessionsWindow'

    list_sessions = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__fill_sessions_list()

    def __fill_sessions_list(self):
        # TODO read from config where is AWS file
        AWS_CONFIG_FILE=path.expanduser("~/.aws/config")

        sessions = aws_config.get_profiles(AWS_CONFIG_FILE)

        for session in sessions:
            self.list_sessions.add(self.render_list_row(session=session))

        self.list_sessions.show_all()

    def render_list_row(self, **kwargs):
        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        row.add(hbox)

        cell = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        hbox.pack_start(cell, True, True, 0)

        session_label = Gtk.Label(kwargs['session'])
        cell.pack_start(session_label, True, True, 0)
        #vbox.pack_start(test_label, True, True, 0)

        # kreiraj progressbar ako session traje, ako netraje button za refresh session
        switch = Gtk.Switch()
        switch.props.valign = Gtk.Align.CENTER
        hbox.pack_start(switch, False, True, 0)
        return row
